<html>
<head>
    <title>Bitmarket Test</title>
    <link rel="stylesheet" type="text/css" media="screen" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <script src="bower_components/angular/angular.min.js"></script>
    <script src="js/features/bit-market-factory.js"></script>
    <script src="js/features/bit-market-controller.js"></script>
    <script src="js/features/bit-market-app.js"></script>
</head>
<body>
<div class="row" ng-app="bitMarket">
    <div class="col-xs-12" ng-controller="bitMarketController">
        <div class="col-xs-6">
            <h1>Bitmarket Test</h1>
            <label for="bitcoin-index">Enter block index</label>
            <input ng-model="blockIndex" id="bitcoin-index" class="form-control" type="text">
            <br />
            <button class="btn btn-success col-xs-12"
                    ng-click="getBlock()"
                    ng-disabled="blockIndex.length == 0">
                Submit
            </button>
        </div>
        <div class="col-xs-6" ng-if="errors.length == 0 && block.block_hash">
            <h1>Block Index: <% blockIndex %></h1>
            <label for="bitcoin-hash">Block Hash</label>
            <p><% block.block_hash %></p>
            <label for="previous-hash">Previous Hash</label>
            <p><% block.previous_hash %></p>
            <label for="next-hash">Next Hash</label>
            <p><% block.next_hash %></p>
        </div>
        <div class="col-xs-6" ng-if="errors.length > 0">
            <h2>Could not resolve block details</h2>
            <p ng-repeat="error in errors track by $index">
                <% error.data.error %>
            </p>
        </div>
    </div>
</div>
</body>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</html>