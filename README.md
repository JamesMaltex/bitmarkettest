# Bitmarket Test

This repository contains a small project built using Laravel and Angular. The application can access the bitcoind daemon and query a blocks' details for a given index.
### Install
Clone the package
```bash
git clone git@bitbucket.org:JamesMaltex/bitmarkettest.git
```
### Install dependencies
```bash
// Navigate to project root dir
cd bitmarkettest

// Install composer dependencies
composer install

// Move into the public folder and install bower components
cd public
bower install
```
N.B. You may receieve this warning:

"(node:11232) fs: re-evaluating native module sources is not supported. If you are using the graceful-fs module, please update it to a more recent version."

This is no problem; the install will have still been successful.

### Update config settings
There is a file in the config directory where you will need to set your configuration details for the bitcoin daemon. Simply use your preferred editor to change the values in 'config/bitcoin.php'.

```php
return [

    /**
     * User and password should match the values in
     * ~/.bitcoin/bitcoin.conf
     */
    'user' => 'user',
    'password' => 'user.',
    'host' => '127.0.0.1',
    'port' => '8332'
];
```

### Update environment settings
Laravel relies on the PHPENV library to manage it's settings. Although we do not require many of them for this application, they still need to be available for the app to run. To do so, navigate 
to the root folder and rename ".env.example" to ".env". Finally, we generate the unique application key.

```bash
cd bitmarket 

php artisan key:generate
```

### Run the application
To run the application, we can use the built in server provided with laravel.
```bash
cd bitmarkettest

// Run the server
php artisan serve
```
If all is working, you should be able to navigate to http://localhost:8000.

### Notes
As laravel was used, there are many files that are unrelated to the work created for this test. The best places to check for my custom work will be:

1) 'packages' - This is the main bulk of PHP work that interacts with the bitcoin daemon.

2) 'app/Http/Controllers/BitcoinController.php' - This is where the BitcoinD class is instantiated and used to query all the data.

3) 'public/js/features' - The front-end angular code used for displaying the data.

4) 'resources/views/bitmarket.blade.php' - The html markup for the app.

### To Do
I have implemented the web client using an interface. I have started work on a CommandLineClient that will interact directly with the bitcoind daemon using exec() as opposed to using the JSONRPC.