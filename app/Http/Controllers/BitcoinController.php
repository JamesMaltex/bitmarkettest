<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maltex\BitcoinDaemon\BitcoinD;
use Maltex\BitcoinDaemon\Client\WebClient;
use Mockery\Exception;

class BitcoinController extends Controller
{
    /**
     * Main web view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function web()
    {
        return view('bitmarket');
    }

    /**
     * Fetch block details given it's index
     *
     * @param $index
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBlock($index)
    {
        $client = new WebClient(
            config('bitcoin.user'),
            config('bitcoin.password'),
            config('bitcoin.host'),
            config('bitcoin.port')
        );
        $bitcoinD = new BitcoinD($client);

        if(! $bitcoinD->checkDaemon()) {
            return response()->json([
                'error' => 'Bitcoin daemon has not been started or is not installed'], 500
            );
        }

        try {
            $bitcoinD->getBlock((int) $index);

            return response()->json([
                'block' => [
                    'block_hash' => $bitcoinD->getHash(),
                    'previous_hash' => $bitcoinD->getPreviousHash(),
                    'next_hash' => $bitcoinD->getNextHash(),
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Failed to fetch block details: ' . $e->getMessage()], 500
            );
        }
    }
}
