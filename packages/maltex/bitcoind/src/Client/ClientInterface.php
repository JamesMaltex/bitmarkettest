<?php

namespace Maltex\BitcoinDaemon\Client;

/**
 * Interface ClientInterface
 */
interface ClientInterface
{
    /**
     * Checks if bitcoind is running
     *
     * @return mixed
     */
    public function checkStatus();

    /**
     * Return a block hash by the
     * given index
     *
     * @return mixed
     */
    public function getBlockHash($index);

    /**
     * Get block details by given hash
     *
     * @param $blockHash
     * @return mixed
     */
    public function getBlock($blockHash);
}