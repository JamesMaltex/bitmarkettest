<?php

namespace Maltex\BitcoinDaemon\Client;

use Mockery\Exception;

class WebClient implements ClientInterface {

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $port;

    /**
     * WebClient constructor.
     *
     * @param $user
     * @param $password
     * @param $host
     * @param $port
     */
    public function __construct($user, $password, $host, $port)
    {
        $this->user = $user;
        $this->password = $password;
        $this->host = $host;
        $this->port = $port;
    }

    /**
     * Mark a curl request to
     *
     * @param $command
     * @param null $params
     * @return array
     */
    public function makeRequest($command, $params = null)
    {
        $request = json_encode([
            'method' => $command,
            'params' => $params,
            'id' => rand()
        ]);

        $curl = curl_init("http://{$this->host}:{$this->port}");
        $options = array(
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_USERPWD => $this->user . ':' . $this->password,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_HTTPHEADER => array('Content-type: application/json'),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $request
        );

        curl_setopt_array($curl, $options);
        $response = json_decode(curl_exec($curl), true);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if($status == 401) {
            throw new \Exception('Failed to authorise. Please check the details in bitcoin.conf');
        }

        if($response['error']) {
            throw new \Exception('The request failed with the following error: ' . $response['error']['message']);
        }

        return $response;
    }

    /**
     * @inheritdoc
     */
    public function checkStatus()
    {
        try {
            $response = $this->makeRequest('getinfo');

            return $response['result'] ?: false;
        } catch (\Exception $e) {
            throw $e;          
        }
    }

    /**
     * @inheritdoc
     */
    public function getBlockHash($index)
    {       
        try {
            $response = $this->makeRequest('getblockhash', [$index]);

            return $response['result'];
        } catch (\Exception $e) {
            throw $e;            
        }
    }

    /*
     * @inheritdoc
     */
    public function getBlock($blockHash)
    {        
        try {
            $response = $this->makeRequest('getblock', [$blockHash]);

            return $response['result'];
        } catch (\Exception $e) {
            throw $e;          
        }
    }
}