<?php

namespace Maltex\BitcoinDaemon\Client;

class CommandLineClient implements ClientInterface {

    /**
     * Run getnfo command to check if
     * bitcoin is running
     *
     * @return boolean
     */
    public function checkStatus()
    {
        return ! empty($this->runCommand('bitcoin-cli getinfo'));
    }

    /**
     * Execute command
     *
     * @param $command
     * @return mixed
     */
    private function runCommand($command)
    {
        $output = [];
        exec($command, $output);
        return $output;
    }

    public function getBlock($blockHash)
    {
        // TODO: Implement getBlock() method.
    }

    public function getBlockHash($index)
    {
        // TODO: Implement getBlockHash() method.
    }
}