<?php

namespace Maltex\BitcoinDaemon;

use Maltex\BitcoinDaemon\Client\ClientInterface;

class BitcoinD {

    /**
     * The client that makes requests
     * to the bitcoind daemon
     *
     * @var ClientInterface
     */
    private $client;

    /**
     * The queries blocks details
     *
     * @var array
     */
    private $blockDetails;

    /**
     * Response array keys
     */
    const HASH_KEY = 'hash';
    const PREVIOUS_KEY = 'previousblockhash';
    const NEXT_KEY = 'nextblockhash';

    /**
     * BitcoinD constructor.
     *
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Have the client test if
     * the daemon is running
     *
     * @return mixed
     */
    public function checkDaemon()
    {
        return $this->client->checkStatus();
    }

    /**
     * Fetch a block by it's index,
     * query the blocks hash and then
     * fetch the block details
     *
     * @param $index
     * @return mixed
     */
    public function getBlock($index)
    {        
        try {
            $blockHash = $this->client->getBlockHash($index);
            $this->blockDetails = $this->client->getBlock($blockHash);

            return $this;
        } catch (\Exception $e) {            
            throw $e;          
        }  
    }

    /**
     * Return all block details
     *
     * @return mixed
     */
    public function getBlockDetails()
    {
        return $this->blockDetails;
    }

    /**
     * Fetch the blocks previous key
     *
     * @return bool|mixed
     */
    public function getHash()
    {
        if(! $this->blockDetails) {
            return false;
        }

        return $this->blockDetails[BitcoinD::HASH_KEY];
    }

    /**
     * Fetch the blocks previous key
     *
     * @return bool|mixed
     */
    public function getPreviousHash()
    {
        if(! $this->blockDetails) {
            return false;
        }

        return $this->blockDetails[BitcoinD::PREVIOUS_KEY];
    }

    /**
     * Fetch the blocks next key
     *
     * @return bool|mixed
     */
    public function getNextHash()
    {
        if(! $this->blockDetails) {
            return false;
        }

        return $this->blockDetails[BitcoinD::NEXT_KEY];
    }
}