angular.module('bitMarketController', [])

    .controller('bitMarketController', function ($scope, BitMarket) {

        $scope.block = {};
        $scope.blockIndex = null;
        $scope.errors = [];

        /**
         * Fetch the block details
         */
        $scope.getBlock = function() {
            $scope.errors = [];

            // validate the value is a number
            if(isNaN($scope.blockIndex)) {
                $scope.errors.push({ data : { error : 'Index has to be numeric' } });
                return false;
            }

            BitMarket.getBlock($scope.blockIndex)
                .then(function(response) {
                    $scope.block = response.data.block;
                },
                function(error) {
                    console.log(error);
                    $scope.errors.push(error);
                });
        }
    });
