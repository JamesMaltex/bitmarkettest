angular.module('bitMarket', [
    'bitMarketController',
    'bitMarketFactory'
], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});
