angular.module('bitMarketFactory', [])


    .factory('BitMarket', function ($http) {

        var factory = {
            getBlock: getBlock,
        };

        return factory;

        function getBlock(id) {
            return $http.get('/block/' + id);
        }
    });